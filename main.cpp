#include <map>
#include <string>
#include <iostream>
#include <typeinfo>
#include <climits>
#include "Clases/Fecha.h"
#include "Clases/DtClase.h"
#include "Clases/DtSocio.h"
#include "Clases/Socio.h"
#include "Clases/Clase.h"
#include "Clases/DtEntrenamiento.h"
#include "Clases/DtSpinning.h"
#include "Clases/Entrenamiento.h"
#include "Clases/Spinning.h"

using std::string;
using std::getline;
using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::stoi;

// Menu
void menu();
void printMenu();

// Menu wrapper
// Se crean estas funciones para no sobrecargar la funcion menu() con codigo de
// obtencion de datos, o de impresion en pantalla.
void handleOption1();
void handleOption2();
void handleOption3();
void handleOption4();
void handleOption5();
void handleOption6();

// Funcionalidades definidas en la letra del obligatorio
void agregarSocio(string ci, string nombre);
void agregarClase(DtClase& clase);
void agregarInscripcion(string ciSocio, int idClase, Fecha fecha);
void borrarInscripcion(string ciSocio, int idClase);
DtSocio** obtenerInfoSociosPorClase (int idClase, int& cantSocios);
DtClase& obtenerClase(int idClase);

// funciones auxiliares
int readInt();
void handleExceptions(const string &eWhat);

// Sobrecarga de operadores
std::ostream& operator <<(std::ostream& out, DtClase& dtc);
std::ostream& operator <<(std::ostream& out, DtSocio& dts);

map<string, Socio *> colSocios;
map<int, Clase *> colClases;

int main()
{
    // Iniciamos el menu principal del programa
    menu();

    return 0;
}

void menu()
{
    string opt;

    do
    {
        printMenu();

        cin >> opt;

        // Leemos la opcion de la entrada estandar y guardamos lo que se recibe en un string.
        // Utilizamos una secuencia de if/else debido a que la estructura de control 'switch'
        // solo acepta enteros como posibles valores.
        if (opt == "1") {
            handleOption1();
        }
        else if (opt == "2") {
            handleOption2();
        }
        else if (opt == "3") {
            handleOption3();
        }
        else if (opt == "4") {
            handleOption4();
        }
        else if (opt == "5") {
            handleOption5();
        }
        else if (opt == "6") {
            handleOption6();
        }

    } while (opt != "7");
}

void printMenu()
{
    cout << endl;
    cout << "Obligatorio 1" << endl;
    cout << "Grupo: 1" << endl << endl;
    cout << "Opciones:" << endl;
    cout << "    1- Agregar socio" << endl;
    cout << "    2- Agregar clase" << endl;
    cout << "    3- Agregar inscripción" << endl;
    cout << "    4- Borrar inscripción" << endl;
    cout << "    5- Obtener información de los socios de una clase" << endl;
    cout << "    6- Obtener información de una clase" << endl;
    cout << "    7- Salir" << endl << endl;
    cout << "  Indique su eleccion: ";
}

void handleOption1()
{
    string ci, nombre;

    cout << "Ingrese la cédula: ";
    cin >> ci;
    cin.ignore(INT_MAX, '\n');

    cout << "Ingrese el nombre: ";
    getline(cin, nombre);

    try
    {
        agregarSocio(ci, nombre);
        cout << endl << "   ---> Socio creado correctamente." << endl;
    }
    catch (std::invalid_argument &e)
    {
        cout << endl << "Error: " << e.what() << endl;
    }
}

void handleOption2()
{
    int id;
    string nombre, optTurno, optActividad;
    Turno turno = Manana;

    try {
        cout << "Ingrese el ID: ";
        id = readInt();
        cin.ignore(INT_MAX, '\n');

        cout << "Ingrese el nombre: ";
        getline(cin, nombre);

        cout << "Ingrese el turno (M: Mañana / t: Tarde / n: Noche): ";
        cin >> optTurno;

        if (optTurno == "t" || optTurno == "T")
            turno = Tarde;

        else if (optTurno == "n" || optTurno == "N")
            turno = Noche;

        cout << "Ingrese la actividad (E: Entrenamiento / s: Spinning): ";
        cin >> optActividad;

        if (optActividad == "s" || optActividad == "S" ) {
            int cantBicis;

            cout << "¿Cuántas bicicletas tiene la clase? ";
            cantBicis = readInt();

            DtSpinning dts = DtSpinning(id, nombre, turno, cantBicis);

            agregarClase(dts);
        }
        else {
            string optEnRambla;
            bool enRambla = true;

            cout << "¿Es en rambla? (S / n): ";
            cin >> optEnRambla;

            if (optEnRambla == "n" || optEnRambla == "N")
                enRambla = false;

            DtEntrenamiento dte = DtEntrenamiento(id, nombre, turno, enRambla);

            agregarClase(dte);
        }

        cout << endl << "   ---> Clase creada correctamente." << endl;
    }
    catch (std::invalid_argument &e) {
        handleExceptions(e.what());
    }
}

void handleOption3()
{
	string ciSocio;
	int idClase,dia,mes,anio;
	Fecha fecha;

    try {
        cout << "Ingrese cédula del Socio: ";
        cin >> ciSocio;

        cout << "Ingrese el identificador de la Clase: ";
        idClase = readInt();

        cout << "Ingrese el día de la Clase: ";
        dia = readInt();

        cout << "Ingrese el mes de la Clase: ";
        mes = readInt();

        cout << "Ingrese el año de la Clase: ";
        anio = readInt();

        fecha = Fecha(dia,mes,anio);

        agregarInscripcion(ciSocio,idClase,fecha);
        cout << endl << "   ---> Inscripción creada correctamente." << endl;
    }
    catch (std::invalid_argument &e){
        // Casteamos el mensaje de la excepcion a string para poder preguntar si es una excepcion lanzada por
        // la funcion 'std::stoi()'. En caso positivo, se muestra un error de que el dato ingresado debe ser numerico.
        if ( (string) e.what() == "stoi") {
            cout << endl << "   ---> Error: el dato ingresado debe ser numérico." << endl;
        }
        else {
            cout << endl << "   ---> Error: " << e.what() << endl;
        }
    }
}

void handleOption4()
{
   string ciSocio;
   int idClase;

    try {
       cout << "Ingrese la cédula del Socio que desea desinscribir: ";
       cin >> ciSocio;

       cout << "Ingrese identificador de la Clase a la cual desea darse de baja: ";
       idClase = readInt();

	   borrarInscripcion(ciSocio, idClase);
       cout << endl << "   ---> Inscripción borrada correctamente." << endl;
	}
	catch (std::invalid_argument &e){
        handleExceptions(e.what());
	}
}

void handleOption5() {
    int cantS;
    int idClase;

    try {
        cout << "Ingrese el ID de la Clase: ";
        idClase = readInt();

        DtSocio** dcss = obtenerInfoSociosPorClase(idClase, cantS);

        cout << endl << "   ---> Se encontró/aron " << cantS << " socio/s en la clase ID " << idClase << ":" << endl;
        for (int i = 0; i < cantS; i++) {
            cout << "           " << *dcss[i];
        }
    }
    catch (std::invalid_argument &e) {
        handleExceptions(e.what());
    }
}
void handleOption6()
{
    int idClase;

    try {
        cout << "Ingrese el ID de la Clase: ";
        idClase = readInt();

        DtClase& dt = obtenerClase(idClase);
        cout << dt;
    }
    catch (std::invalid_argument &e) {
        handleExceptions(e.what());
    }
}


void agregarSocio(string ci, string nombre)
{
    if (colSocios[ci] != nullptr)
        throw std::invalid_argument("El Socio ya existe en el sistema.");
    
    Socio* s = new Socio(ci, nombre);
    colSocios[ci] = s;
}

void agregarClase(DtClase& clase)
{
    if (colClases[clase.getId()] != nullptr)
        throw std::invalid_argument("La Clase ya existe en el sistema.");

    try {
        auto dte = dynamic_cast<DtEntrenamiento &>(clase);
        Entrenamiento* c = new Entrenamiento(dte.getId(), dte.getNombre(), dte.getTurno(), dte.getEnRambla());
        colClases[dte.getId()] = c;
        return;

    } catch (std::bad_cast &e) {}

    try {
        auto dts = dynamic_cast<DtSpinning &>(clase);
        Spinning* c = new Spinning(dts.getId(), dts.getNombre(), dts.getTurno(), dts.getCantBicicletas());
        colClases[dts.getId()] = c;
        return;

    } catch (std::bad_cast &e) {}
}

void agregarInscripcion(string ciSocio, int idClase, Fecha fecha)
{
    // Corroboramos la existencia del socio con clave 'ciSocio' en el map de socios.
    Socio *socio =  colSocios[ciSocio];
	if (socio == nullptr) {
	    throw std::invalid_argument("El Socio no existe");
	}

	// Corroboramos la existencia de la clase con clave 'idClase' en el map de clases.
	Clase *clase = colClases[idClase];
	if (clase == nullptr) {
	    throw std::invalid_argument("La Clase no existe");
	}

	// Si el socio y la clase existen, se verifica el cupo.
    if (clase->cupo() < 1) {
        throw std::invalid_argument("No hay cupo disponible");
    }

    // Si hay cupo suficiente, se corrobora que el socio no este inscripro a la clase.
    if (clase->existeSocioInscripto(socio)) {
        throw std::invalid_argument("El Socio ya esta inscripto a la Clase");
    }

    Inscripcion* ins = new Inscripcion(fecha, socio);
    clase->setInscripcion(ins);
}

void borrarInscripcion(string ciSocio, int idClase)
{
    // Corroboramos la existencia del socio con clave 'ciSocio' en el map de socios.
    Socio *socio =  colSocios[ciSocio];
    if (socio == nullptr) {
        throw std::invalid_argument("El Socio no existe");
    }

    // Corroboramos la existencia de la clase con clave 'idClase' en el map de clases.
    Clase *clase = colClases[idClase];
    if (clase == nullptr) {
        throw std::invalid_argument("La Clase no existe");
    }


    Inscripcion *ins;
    int i = 0;
    bool found = false;

    while (i < MAX_INSCRIPCION && ! found) {
        ins = clase->getInscripcion(i);
        if (ins != nullptr && ins->getSocio() == socio) {
            delete ins;
			clase->setInscripcion(nullptr, i);
			found = true;
        }

        i++;
    }

    if ( ! found) {
        throw std::invalid_argument("No se encotró una inscripción para el socio " + ciSocio + " a la clase " + clase->getNombre());
    }
}

DtSocio** obtenerInfoSociosPorClase (int idClase, int& cantSocios)
{
	cantSocios = 0;
    Clase *clase = colClases[idClase];
    if (clase == nullptr) {
        throw std::invalid_argument("La clase no existe");
    }

	Inscripcion *ins = nullptr;
    int i = 0;
	while (i < MAX_INSCRIPCION) {
        ins = clase->getInscripcion(i);
        if (ins != nullptr) {
            cantSocios++;
        }
		i++;
    }

    DtSocio** retorno = new DtSocio*[cantSocios];
	i = 0;
	Socio *socio = nullptr;
	string ciSocio, nombre;
    while (i < MAX_INSCRIPCION) {
        ins = clase->getInscripcion(i);
        if (ins != nullptr) {
            socio = ins->getSocio();
			ciSocio = socio->getCi();
			nombre = socio->getNombre();
			DtSocio *dts = new DtSocio(ciSocio,nombre);
			retorno[i]= dts;
        }

        i++;
    }

    return retorno;
}

DtClase& obtenerClase(int idClase)
{
	string nombre;
	Turno turno;
	Clase *clase = colClases[idClase];
    if (clase == nullptr) {
        throw std::invalid_argument("La Clase no existe");
    }
	
	nombre = clase->getNombre();
	turno = clase->getTurno();

    auto claseE = dynamic_cast<Entrenamiento *>(clase);
    if (claseE != nullptr) {
        bool enRambla = claseE->getEnRambla();
        DtEntrenamiento *dtEnt = new DtEntrenamiento(idClase, nombre, turno, enRambla);
        return *dtEnt;
    }

    auto claseS = dynamic_cast<Spinning *>(clase);
    if (claseS != nullptr) {
        int cantBicis = claseS->getCantBicicletas();
        DtSpinning *dtSp = new DtSpinning (idClase, nombre, turno, cantBicis);
        return *dtSp;
    }
}

// Funciones auxiliares
int readInt() {

    /**
     * Lee de la entrada estandar un string, y lo convierte a 'int'.
     * En caso de no poder realizar la conversion, lanza una excepcion invalid_argument con
     * operacion .what() = 'stoi' (comportamiento propio de la funcion 'stoi()').
     */

    string s;
    cin >> s;
    return stoi(s);
}

void handleExceptions(const string &eWhat) {

    /**
     * Toma un e.what() (donde 'e' es una excepcion del tipo std::invalid argument), y verifica
     * si dicha excepcion fue lanzada por la funcion 'std::stoi()'. En caso positivo se muestra un
     * error generico de que se esperaba un valor numerico; sino, se muestra el e.what() recibido
     * por parametro.
     */

    if (eWhat == "stoi") {
        cout << endl << "   ---> Error: el dato ingresado debe ser numérico." << endl;
    }
    else {
        cout << endl << "   ---> Error: " << eWhat << endl;
    }
}

// Sobrecarga de operadores
std::ostream& operator <<(std::ostream& out, DtClase& dtc) {
    string turno;

    switch (dtc.getTurno()) {
        case Manana: {
            turno = "Mañana";
            break;
        }
        case Tarde: {
            turno = "Tarde";
            break;
        }
        case Noche: {
            turno = "Noche";
            break;
        }
    }

    out << endl
        << "   ID Clase: " << dtc.getId() << endl
        << "   Nombre: " << dtc.getNombre() << endl
        << "   Turno: " << turno << endl;

    try {
        auto dte = dynamic_cast<DtEntrenamiento &>(dtc);
        out << "   En rambla? ";

        if (dte.getEnRambla())
            out << "Si" << endl;

        else
            out << "No" << endl;

        return out;
    }
    catch (std::bad_cast &e) {}

    try {
        auto dts = dynamic_cast<DtSpinning &>(dtc);
        out << "   Cantidad de bicicletas: " << dts.getCantBicicletas() << endl;
        return out;
    }
    catch (std::bad_cast &e) {}
}

std::ostream& operator <<(std::ostream& out, DtSocio& dts) {
    out << "Ci: " << dts.getCi() << ", Nombre: " << dts.getNombre() << endl;
}
