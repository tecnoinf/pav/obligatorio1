//
// Created by usu-ubuntusu on 08/04/18.
//

#ifndef OBLIGATORIO1_DTENTRENAMIENTO_H
#define OBLIGATORIO1_DTENTRENAMIENTO_H

#include "DtClase.h"
#include <string>

using std::string;

class DtEntrenamiento: public DtClase{

private:
    bool enRambla;

public:

    //Constructor
    DtEntrenamiento();
    DtEntrenamiento(int id, string nombre, Turno turno, bool enRambla);

    //Getters
    bool getEnRambla();

};

#endif //OBLIGATORIO1_DTENTRENAMIENTO_H