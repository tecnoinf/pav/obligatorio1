//
// Created by sebiitta on 07/04/18.
//

#include "DtClase.h"

#include <string>
#include <iostream>
#include "../Tipos/Tipos.h"
#include "DtEntrenamiento.h"
#include "DtSpinning.h"

using std::string;
using std::endl;

//Constructores
DtClase::DtClase() {
    this->id = 0;
    this->nombre = "";
    this->turno = Manana;
}

DtClase::DtClase(int id, string nombre, Turno turno) {
    this->id = id;
    this->nombre = nombre;
    this->turno = turno;

}

//Getters
int DtClase::getId(){
    return id;
}

string DtClase::getNombre(){
    return nombre;
}

Turno DtClase::getTurno(){
    return turno;
}

// Destructor
DtClase::~DtClase() = default;
