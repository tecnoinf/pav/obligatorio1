//
// Created by sebiitta on 07/04/18.
//

#ifndef PARTE1_DTCLASE_H
#define PARTE1_DTCLASE_H

#include <string>
#include "../Tipos/Tipos.h"

using std::string;

class DtClase {
private:
    int id;
    string nombre;
    Turno turno;

public:

    //Constructores
    DtClase();
    DtClase(int id, string nombre, Turno turno);

    //Getters
    int getId();
    string getNombre();
    Turno getTurno();

    // Destructor
    virtual ~DtClase();
};



#endif //PARTE1_DTCLASE_H
