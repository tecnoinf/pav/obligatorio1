#include <stdexcept>
#include "Spinning.h"

using std::to_string;

// Constructores
Spinning::Spinning() : Clase() {
    this->cantBicicletas = MAX_INSCRIPCION;
}

Spinning::Spinning(int id, string nombre, Turno turno, int cantBicicletas) : Clase(id, move(nombre), turno) {
    this->setCantBicicletas(cantBicicletas);
}

// Getters
int Spinning::getCantBicicletas() {
    return this->cantBicicletas;
}

// Setters
void Spinning::setCantBicicletas(int cant) {
    if (cant > MAX_INSCRIPCION){
        throw std::invalid_argument ("No puede haber más de " + to_string(MAX_INSCRIPCION) + " bicicletas.");
    }

    this->cantBicicletas = cant;
}

// Operaciones
int Spinning::cupo() {
    int cont = 0;  // Contador de inscriptos

    for (int i = 0; i < MAX_INSCRIPCION; i++)
    {
        if (this->getInscripcion(i) != nullptr)
            cont++;

    }

    // El cupo esta definido por la cantidad de bicicletas (con un maximo MAX_INSCRIPTOS) menos la
    // cnatidad de inscriptos actualmente.
    return this->cantBicicletas - cont;
}

Spinning::~Spinning() = default;
