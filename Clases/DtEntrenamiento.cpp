//
// Created by usu-ubuntusu on 08/04/18.
//
#include <string>
#include <iostream>
#include "DtClase.h"
#include "DtEntrenamiento.h"


using std::string;
using std::endl;

//Constructor
DtEntrenamiento::DtEntrenamiento() : DtClase() {

    this->enRambla = true;
}

DtEntrenamiento::DtEntrenamiento(int id, string nombre, Turno turno, bool enRambla) : DtClase(id,nombre,turno) {

    this->enRambla = enRambla;
}

//Getters
bool DtEntrenamiento::getEnRambla(){

    return enRambla;
}

