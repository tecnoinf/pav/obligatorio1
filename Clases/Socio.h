//
// Created by sebiitta on 28/03/18.
//

#ifndef PARTE1_SOCIO_H
#define PARTE1_SOCIO_H

#include <string>

using std::string;

class Socio {
private:
    string ci;
    string nombre;

public:
    // Constructores
    Socio();
    Socio(string ci, string nombre);

    // Getters
    string getCi();
    string getNombre();

    // Setters
    void setCi(string ci);
    void setNombre(string nombre);

    // Operaciones
    // Aqui iria definida la funcion que retorna un DtSocio con
    // con los datos del socio
    //DtSocio getDtSocio();

    // Destructor
    ~Socio();
};


#endif //PARTE1_SOCIO_H
