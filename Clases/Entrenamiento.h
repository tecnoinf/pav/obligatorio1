//
// Created by usu-ubuntusu on 08/04/18.
//

#ifndef OBLIGATORIO1_ENTRENAMIENTO_H
#define OBLIGATORIO1_ENTRENAMIENTO_H

#include "Clase.h"
#include <string>

using std::string;

class Entrenamiento: public Clase{

private:
    bool enRambla;

public:

    //Contructores
    Entrenamiento();
    Entrenamiento(int id, string nombre, Turno turno,bool enRambla);

    //Getters
    bool getEnRambla();

    //Setters
    void setEnRambla(bool enRambla);

    //Operacion
    int cupo() override;

    //Destructor
    ~Entrenamiento();

};

#endif //OBLIGATORIO1_ENTRENAMIENTO_H
