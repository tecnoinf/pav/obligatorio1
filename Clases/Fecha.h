//
// Created by usu-ubuntusu on 01/04/18.
//

#ifndef LABORATORIO1_FECHA_H
#define LABORATORIO1_FECHA_H

#include <string>

using std::string;

class Fecha {

private:
    int dia;
    int mes;
    int anio;

public:

    //Constructores
    Fecha();
    Fecha(int dia, int mes, int anio);

    //Getter
    int getDia();
    int getMes();
    int getAnio();
    string getFecha();
};

#endif //LABORATORIO1_FECHA_H
