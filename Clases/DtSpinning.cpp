//
// Created by sebiitta on 08/04/18.
//
#include "DtClase.h"
#include "DtSpinning.h"
#include <string>

using std::string;

//Constructores

DtSpinning::DtSpinning() : DtClase() {

    this->cantBicicletas = 50;
}

DtSpinning::DtSpinning(int id, string nombre, Turno turno,int cantBicicletas) :DtClase(id,nombre,turno) {

    this->cantBicicletas = cantBicicletas;
}

//Getters

int DtSpinning::getCantBicicletas(){
    return cantBicicletas;
}