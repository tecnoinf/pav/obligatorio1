//
// Created by usu-ubuntusu on 01/04/18.
//

#include "Inscripcion.h"

// Constructores
Inscripcion::Inscripcion() {
    this->fecha = Fecha();
    this->socio = nullptr;
}

Inscripcion::Inscripcion(Fecha fecha, Socio *socio) {
    this->fecha = fecha;
    this->socio = socio;
}

// Getters
Fecha Inscripcion::getFecha() {
    return this->fecha;
}

Socio *Inscripcion::getSocio() {
    return this->socio;
}

// Setters
void Inscripcion::setFecha(Fecha f) {
    this->fecha = f;
}

void Inscripcion::setSocio(Socio * s){
    this->socio = s;

}

// Destructor
Inscripcion::~Inscripcion() = default;
