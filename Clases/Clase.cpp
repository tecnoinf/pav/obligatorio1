#include "Clase.h"
#include <utility>
#include <stdexcept>

using std::move;

// Constructores
Clase::Clase() {
    this->id = 0;
    this->nombre = "";
    this->turno = Manana;
//    this->inscriptos[50] = {nullptr};  // Inicializa el array completo con NULL en todas sus posiciones???.
}

Clase::Clase(int id, string nombre, Turno turno) {
    this->id = id;
    this->nombre = move(nombre);
    this->turno = turno;
    for (int i = 0; i < MAX_INSCRIPCION; i++) {
        this->inscriptos[i] = nullptr;
    }
//    this->inscriptos[50] = {nullptr};  // Inicializa el array completo con NULL en todas sus posiciones???.
}

Clase::Clase(int id, string nombre, Turno turno, Inscripcion * ins) {
    this->id = id;
    this->nombre = move(nombre);
    this->turno = turno;
    this->inscriptos[0] = ins;
}

Clase::Clase(int id, string nombre, Turno turno, Inscripcion* ins[]) {
    this->id = id;
    this->nombre = move(nombre);
    this->turno = turno;

    for (int i = 0; i < 50; i++)
    {
        this->inscriptos[i] = ins[i];
    }
}

// Getters
int Clase::getId() {
    return this->id;
}

string Clase::getNombre() {
    return this->nombre;
}

Turno Clase::getTurno() {
    return this->turno;
}

Inscripcion* Clase::getInscripcion(int index) {
    if (index < 0 || index > MAX_INSCRIPCION)
    {
        throw std::invalid_argument("El inidice indicado esta fuera de los limites.");
    }
    return this->inscriptos[index];
}

// Setters
void Clase::setId(int id) {
    this->id = id;
}

void Clase::setNombre(string nombre) {
    this->nombre = move(nombre);
}

void Clase::setTurno(Turno turno) {
    this->turno = turno;
}

void Clase::setInscripcion(Inscripcion* ins) {
    int i = 0;
    while (i < MAX_INSCRIPCION && this->inscriptos[i] != nullptr)
        i++;

    if (i != MAX_INSCRIPCION)
        this->inscriptos[i] = ins;

}

void Clase::setInscripcion(Inscripcion *ins, int indice) {
    this->inscriptos[indice] = ins;
}

// Operaciones
bool Clase::existeSocioInscripto(Socio *socio) {
    int i = 0;
    bool found = false;

    while (i < MAX_INSCRIPCION && ! found) {
        found = (this->inscriptos[i] != nullptr) && (this->inscriptos[i]->getSocio() == socio);
        i++;
    }

    return found;
}

// Destructor
Clase::~Clase() {
    for (auto &i : this->inscriptos) {
        delete i;
    }
}
