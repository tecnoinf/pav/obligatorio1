//
// Created by sebiitta on 08/04/18.
//

#ifndef PARTE1_DTSPINNING_H
#define PARTE1_DTSPINNING_H


#include "DtClase.h"
#include <string>

using std::string;

class DtSpinning: public DtClase {
private:
    int cantBicicletas;

public:
    //constructores
    DtSpinning();
    DtSpinning(int id, string nombre, Turno turno, int cantBicicletas);

    //Getters
    int getCantBicicletas();
};






#endif //PARTE1_DTSPINNING_H
