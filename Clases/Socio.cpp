#include "Socio.h"
#include <utility>

using std::move;

// Constructores
Socio::Socio() {
    this->ci = "";
    this->nombre = "";
}

Socio::Socio(string ci, string nombre) {
    this->ci = move(ci);
    this->nombre = move(nombre);
}

// Getters
string Socio::getCi() {
    return this->ci;
}

string Socio::getNombre() {
    return this->nombre;
}

// Setters
void Socio::setCi(string ci) {
    this->ci = move(ci);
}

void Socio::setNombre(string nombre) {
    this->nombre = move(nombre);
}

// Operaciones
// Aqui iria definida la funcion que retorna un DtSocio con
// con los datos del socio
//DtSocio getDtSocio() {}

// Destructor
//Socio::~Socio() {}
Socio::~Socio() = default;
