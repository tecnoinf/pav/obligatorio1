//
// Created by usu-ubuntusu on 08/04/18.
//

#include "Entrenamiento.h"

#include "Clase.h"
#include <string>

using std::string;

//Contructores
Entrenamiento::Entrenamiento() : Clase(){

    this->enRambla = true;
}

Entrenamiento::Entrenamiento(int id, string nombre, Turno turno,bool enRambla) : Clase(id,nombre,turno){

    this->enRambla = enRambla;
}


//Getters
bool Entrenamiento::getEnRambla(){

    return enRambla;
}

//Setters
void Entrenamiento::setEnRambla(bool enRambla){

    this->enRambla = enRambla;
}


//Operacion
int Entrenamiento::cupo(){

    int max;
    int cont = 0;

    if(enRambla){
        max = 20;
    } else{
        max = 10;
    }


    for(int i=0 ; i<max ; i++){
        if (getInscripcion(i)!= nullptr) {
            cont++;
        }
    }
    return max-cont;
}


//Destructor
Entrenamiento::~Entrenamiento() {};
