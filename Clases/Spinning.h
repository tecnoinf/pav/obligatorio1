//
// Created by sebiitta on 03/04/18.
//

#ifndef PARTE1_SPINNING_H
#define PARTE1_SPINNING_H

#include "Clase.h"

class Spinning: public Clase {
private:
    int cantBicicletas;

public:
    // Constructores
    Spinning();
    Spinning(int id, string nombre, Turno turno, int cantBicicletas);

    // Getters
    int getCantBicicletas();

    // Setters
    void setCantBicicletas(int cant);

    // Operacion
    int cupo() override;

    // Destructor
    ~Spinning();
};


#endif //PARTE1_SPINNING_H
