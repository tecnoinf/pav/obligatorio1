//
// Created by sebiitta on 03/04/18.
//

#ifndef PARTE1_CLASE_H
#define PARTE1_CLASE_H

#include <string>
#include "../Tipos/Tipos.h"
#include "Inscripcion.h"

#define MAX_INSCRIPCION 50

using std::string;

class Clase {
private:
    int id;
    string nombre;
    Turno turno;
    Inscripcion* inscriptos[MAX_INSCRIPCION];

public:
    // Constructores
    Clase();
    Clase(int id, string nombre, Turno turno);
    Clase(int id, string nombre, Turno turno, Inscripcion* ins);
    Clase(int id, string nombre, Turno turno, Inscripcion* ins[]);


    // Getters
    int getId();
    string getNombre();
    Turno getTurno();
    Inscripcion* getInscripcion(int index);

    // Setters
    void setId(int id);
    void setNombre(string nombre);
    void setTurno(Turno turno);
    void setInscripcion(Inscripcion* ins);
    void setInscripcion(Inscripcion* ins, int indice);

    // Operaciones
    // Una operacion 'virtual' es una operacion que no se implementa en la propia clase,
    // y se hereda a los hijos. La sintaxis es: 'virtual <tipoRetorno> <nombreFuncion>() = 0'.
    virtual int cupo() = 0;
    bool existeSocioInscripto(Socio *socio);

    // Destructor
    ~Clase();
};


#endif //PARTE1_CLASE_H
