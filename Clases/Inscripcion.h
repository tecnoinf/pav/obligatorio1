//
// Created by usu-ubuntusu on 01/04/18.
//

#ifndef LABORATORIO1_INSCRIPCION_H
#define LABORATORIO1_INSCRIPCION_H

#include "Fecha.h"
#include "Socio.h"


class Inscripcion {
private:
    Fecha fecha;
    Socio *socio;

public:

    //Constructores
    Inscripcion();
    Inscripcion(Fecha fecha, Socio *socio);

    //Getter
    Fecha getFecha();
    Socio *getSocio();

    //Setter
    void setFecha(Fecha f);
    void setSocio(Socio * s);

    //Destructor
    ~Inscripcion();

};


#endif //LABORATORIO1_INSCRIPCION_H
