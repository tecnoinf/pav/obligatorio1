//
// Created by sebiitta on 07/04/18.
//

#ifndef PARTE1_DTSOCIO_H
#define PARTE1_DTSOCIO_H

#include <string>

using std::string;

class DtSocio {

private:
    string ci;
    string nombre;

public:

    //Constructores
    DtSocio();
    DtSocio(string ci, string nombre);

    //Getters
    string getCi();
    string getNombre();
    //DtSocio getDtSocio();

};


#endif //PARTE1_DTSOCIO_H
