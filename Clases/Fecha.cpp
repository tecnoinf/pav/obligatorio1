//
// Created by usu-ubuntusu on 01/04/18.
//

#include "Fecha.h"

// Constructores
Fecha::Fecha() {
    // Se crea con la fecha mas pequenia soportada segun los requerimientos.
    this->dia = 1;
    this->mes = 1;
    this->anio = 1990;
}

#include <string>
#include <stdexcept>

using std::string;
using std::to_string;


Fecha::Fecha(int dia, int mes, int anio) {
    if (dia < 1 || dia > 31)
        throw std::invalid_argument("El día no es válido.");

    if (mes < 1 || mes > 12)
        throw std::invalid_argument("El mes no es válido.");

    if (anio < 1900)
        throw std::invalid_argument("El año no es válido.");

    this->dia = dia;
    this->mes = mes;
    this->anio = anio;
}

// Getters
int Fecha::getDia() {
    return this->dia;
}

int Fecha::getMes() {
    return this->mes;
}

int Fecha::getAnio() {
    return this->anio;
}

string Fecha::getFecha() {

    //string creceFecha[11];
    string d = to_string(dia);
    string m = to_string(mes);
    string a = to_string(anio);
    return d + m + a;
}
