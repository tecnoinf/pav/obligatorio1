//
// Created by sebiitta on 15/04/18.
//

#ifndef PARTE1_B_H
#define PARTE1_B_H

class A;
class C;

class B {
private:
    A *a;
    C *c;

public:
    // Constructor
    B();

    // Getters
    A* getA();
    C* getC();

    // Setters
    void setA(A *a);
    void setC(C *c);

    // Operaciones
    void info();

    // Destructor
    ~B();
};


#endif //PARTE1_B_H
