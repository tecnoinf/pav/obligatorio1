//
// Created by sebiitta on 15/04/18.
//

#include <iostream>
#include "A.h"

using std::cout;
using std::endl;

// Constructor
A::A() {
    this->b = nullptr;
    this->c = nullptr;
}

// Getters
B* A::getB() {
    return this->b;
}

C* A::getC() {
    return this->c;
}

// Setters
void A::setB(B *b) {
    this->b = b;
}

void A::setC(C *c) {
    this->c = c;
}

// Operaciones
void A::info() {
    cout << "Esta es la clase A" << endl;
}

// Destructor
A::~A() = default;
