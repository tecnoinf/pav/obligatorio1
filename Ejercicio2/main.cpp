//
// Created by sebiitta on 15/04/18.
//
#include "A.h"
#include "B.h"
#include "C.h"

int main() {
    A *a = new A();
    B *b = new B();
    C *c = new C();

    a->setB(b);
    a->setC(c);
    b->setA(a);
    b->setC(c);
    c->setA(a);
    c->setB(b);

    a->info();
    b->info();
    c->info();

    delete a;
    delete b;
    delete c;

    return 0;
}
