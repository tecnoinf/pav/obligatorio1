//
// Created by sebiitta on 15/04/18.
//

#ifndef PARTE1_A_H
#define PARTE1_A_H

class B;
class C;

class A {
private:
    B *b;
    C *c;

public:
    // Constructor
    A();

    // Getters
    B* getB();
    C* getC();

    // Setters
    void setB(B *b);
    void setC(C *);

    // Operaciones
    void info();

    // Destructor
    ~A();
};


#endif //PARTE1_A_H
