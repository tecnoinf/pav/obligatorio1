//
// Created by sebiitta on 15/04/18.
//

#ifndef PARTE1_C_H
#define PARTE1_C_H

class A;
class B;

class C {
private:
    A *a;
    B *b;

public:
    // Constructor
    C();

    // Getters
    A* getA();
    B* getB();

    // Setters
    void setA(A *a);
    void setB(B *b);

    // Operaciones
    void info();

    // Destructor
    ~C();
};


#endif //PARTE1_C_H
