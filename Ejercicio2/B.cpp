//
// Created by sebiitta on 15/04/18.
//

#include <iostream>
#include "B.h"

using std::cout;
using std::endl;

// Constructor
B::B() {
    this->a = nullptr;
    this->c = nullptr;
}

// Getters
A* B::getA() {
    return this->a;
}

C* B::getC() {
    return this->c;
}

// Setters
void B::setA(A *a) {
    this->a = a;
}

void B::setC(C *c) {
    this->c = c;
}

// Operaciones
void B::info() {
    cout << "Esta es la clase B" << endl;
}

// Destructor
B::~B() = default;
