All: Clases/Clase.o Clases/DtClase.o Clases/DtEntrenamiento.o Clases/DtSocio.o Clases/DtSpinning.o Clases/Entrenamiento.o Clases/Fecha.o Clases/Inscripcion.o Clases/Socio.o Clases/Spinning.o
	if [ -d dist ]; then rm -Rf dist; fi
	mkdir dist
	g++ -std=c++14 -o dist/gym Clases/Clase.o Clases/DtClase.o Clases/DtEntrenamiento.o Clases/DtSocio.o Clases/DtSpinning.o Clases/Entrenamiento.o Clases/Fecha.o Clases/Inscripcion.o Clases/Socio.o Clases/Spinning.o main.cpp

Clases/Clase.o: Clases/Clase.h Clases/Clase.cpp
	g++ -std=c++14 -c -o Clases/Clase.o Clases/Clase.cpp

Clases/DtClase.o: Clases/DtClase.h Clases/DtClase.cpp
	g++ -std=c++14 -c -o Clases/DtClase.o Clases/DtClase.cpp

Clases/DtEntrenamiento.o: Clases/DtEntrenamiento.h Clases/DtEntrenamiento.cpp
	g++ -std=c++14 -c -o Clases/DtEntrenamiento.o Clases/DtEntrenamiento.cpp

Clases/DtSocio.o: Clases/DtSocio.h Clases/DtSocio.cpp
	g++ -std=c++14 -c -o Clases/DtSocio.o Clases/DtSocio.cpp

Clases/DtSpinning.o: Clases/DtSpinning.h Clases/DtSpinning.cpp
	g++ -std=c++14 -c -o Clases/DtSpinning.o Clases/DtSpinning.cpp

Clases/Entrenamiento.o: Clases/Entrenamiento.h Clases/Entrenamiento.cpp
	g++ -std=c++14 -c -o Clases/Entrenamiento.o Clases/Entrenamiento.cpp

Clases/Fecha.o: Clases/Fecha.h Clases/Fecha.cpp
	g++ -std=c++14 -c -o Clases/Fecha.o Clases/Fecha.cpp

Clases/Inscripcion.o: Clases/Inscripcion.h Clases/Inscripcion.cpp
	g++ -std=c++14 -c -o Clases/Inscripcion.o Clases/Inscripcion.cpp

Clases/Socio.o: Clases/Socio.h Clases/Socio.cpp
	g++ -std=c++14 -c -o Clases/Socio.o Clases/Socio.cpp

Clases/Spinning.o: Clases/Spinning.h Clases/Spinning.cpp
	g++ -std=c++14 -c -o Clases/Spinning.o Clases/Spinning.cpp

clean:
	rm -f Clases/*.o
	rm -f Clases/*.gch
	rm -fr dist

